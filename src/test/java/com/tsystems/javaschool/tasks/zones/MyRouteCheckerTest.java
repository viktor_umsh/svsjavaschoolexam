package com.tsystems.javaschool.tasks.zones;

/**
 * Created by Viktor on 11.09.2017.
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MyRouteCheckerTest {
    private RouteChecker routeChecker = new RouteChecker();
    private int primeMagicConst = 23900533;

    @Test
    public void checkRoute1() throws Exception {
        List<Integer> requestedZones = Arrays.asList(119, 15, 3, 7);
        Assert.assertTrue(routeChecker.checkRoute(state1, requestedZones));
    }

    @Test
    public void checkRoute2() throws Exception {
        List<Integer> requestedZones = Arrays.asList(7, 3, 15, 119, 15, 7, 3, 15);
        Assert.assertTrue(routeChecker.checkRoute(state1, requestedZones));
    }

    @Test
    public void checkRoute3() throws Exception {
        List<Integer> requestedZones = Arrays.asList(15, 119, 7);
        Assert.assertFalse(routeChecker.checkRoute(state1, requestedZones));
    }

    @Test
    public void checkRoute4() throws Exception {
        LinkedList<Integer> requestedZones = new LinkedList<Integer>();
        for (int i = 0; i < 100500; i++) {
            requestedZones.add(primeMagicConst*i);
        }
        Assert.assertTrue(routeChecker.checkRoute(state2, requestedZones));
    }

    @Test
    public void checkRoute5() throws Exception {
        LinkedList<Integer> requestedZones = new LinkedList<Integer>();
        for (int i = 100500-1; i >= 0; i--) {
            requestedZones.add(primeMagicConst*i);
        }
        requestedZones.add(3*primeMagicConst);
        Assert.assertFalse(routeChecker.checkRoute(state1, requestedZones));
    }

    private final List<Zone> state1 = new ArrayList<>();
    private final List<Zone> state2 = new ArrayList<>();

    {
        state1.add(new Zone(15, Arrays.asList(7, 3, 119)));
        state1.add(new Zone(3, Arrays.asList(7)));
        state1.add(new Zone(119, null));
        state1.add(new Zone(7, null));

        for (int i = 0; i < 100500; i++) {
            state2.add(new Zone(i*primeMagicConst, Arrays.asList((i+1)*primeMagicConst, (i+2)*primeMagicConst)));
        }
    }
}
