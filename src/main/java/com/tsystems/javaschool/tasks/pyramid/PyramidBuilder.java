package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) {
            throw new CannotBuildPyramidException("Inputting data is null");
        }
        if (!isCorrectInout(inputNumbers.size())) {
            throw new CannotBuildPyramidException("Can not build pyramid with that count of elements");
        }
        Iterator<Integer> iterator = inputNumbers.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == null) {
                throw new CannotBuildPyramidException("Can not build pyramid with null element");
            }
        }

        Integer[] inputArray = new Integer[inputNumbers.size()];
        inputArray = inputNumbers.toArray(inputArray);
        Arrays.sort(inputArray);

        int last = (int)(Math.sqrt((double)(inputArray.length * 2))); // 1+2+..+last = inputArray.length
        int weight = 2*last-1;
        int high = (weight + 1) / 2;
        int[][] result = new int[high][weight];

        int cursor = 0;
        for (int i = 0; i < high; i++) {
            for (int j = weight/2-i; j <= weight/2+i; j += 2) {
                result[i][j] = inputArray[cursor];
                cursor++;
            }
        }
        return result;
    }

    private boolean isCorrectInout(int size) {
        int base = (int)(Math.sqrt((double)(size*2)));
        return (base*(base+1) == 2*size);
    }
}
